﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour{
	

	public float speed = 6f;
	public float RotateSpeed = 300f;

	private Vector3 movement;
	private Rigidbody playerRigidbody;
	public Transform camera;
	private Vector3 playerOffset;


	void Awake(){
		playerRigidbody = GetComponent<Rigidbody> ();
	}

	void FixedUpdate(){
		//float h = Input.GetAxisRaw ("Horizontal");
		//float v = Input.GetAxisRaw ("Vertical");
		float h = (float)BluetoothReceiver.getInstance().getXAxis();
		float v = (float)BluetoothReceiver.getInstance ().getYAxis ();
		Move (h, v);
	}

	void Move(float h,float v){
		movement.Set (h, 0f, v);
		movement = movement.normalized * speed * Time.deltaTime;
		playerRigidbody.MovePosition (transform.position + movement);
	}
}
