﻿using UnityEngine;
using System.Collections;

public class PianoManager : MonoBehaviour {

	public GameObject keysGroup;
	public GameObject key1;
	public GameObject key2;
	public GameObject key3;
	public GameObject key4;

	private bool playerCollision =false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if (playerCollision) {
			bool pressed = BluetoothReceiver.getInstance ().getBtn1Pressed ();
			CheckKey (key1, pressed);

			pressed = BluetoothReceiver.getInstance ().getBtn2Pressed ();
			CheckKey (key2, pressed);

			pressed = BluetoothReceiver.getInstance ().getBtn3Pressed ();
			CheckKey (key3, pressed);

			//pressed = BluetoothReceiver.getInstance ().getBtn4Pressed ();
			//CheckKey (key4, pressed);
		}
	
	}

	private void CheckKey(GameObject gameObject, bool pressed){
		if (pressed) {
			//TODO: gameObject.GetComponent<Renderer>().material.color = Color (0.777, 08, 0.604);
			gameObject.GetComponent<AudioSource> ().Play ();
		} else {
			//TODO: gameObject.GetComponent<Renderer>().material.color = Color (0f,0f,0f);
		}
			
	}

	void OnCollisionEnter(Collision col){
		Debug.Log ("PianoManager; OnCollisionEnter: " + col.gameObject.name);
		if (col.gameObject.name == "Player") {
			playerCollision = true;
			keysGroup.SetActive (true);
		}
	}

	void OnCollisionExit(Collision col){
		Debug.Log ("PianoManager; OnCollisionExit: " + col.gameObject.name);
		if (col.gameObject.name == "Player") {
			playerCollision = false;
			keysGroup.SetActive (false);
		}
	}
}
